#! /usr/bin/env python

# ******************************************************************************
# This file is part of Cadavreski.
#
#   Cadavreski is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Cadavreski is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
# ******************************************************************************

import sys
import argparse	# option management, close to C++/boost library (?)

# ========================================
def main( o_args ):
	SomePplHere();
	pass

	return

# ----------------------------------------
def defineOptions():
	descr  = "Cadavreski will do something, sometimes, don't know yet ;)\n"
	descr += "\tIt is more an exercise/game than a project with a goal, at least when it starts."
	o_parser = argparse.ArgumentParser( description=descr )

	return o_parser

def analyseOptions( o_args ):
	pass

def SomePplHere():
	return -1;

# ----------------------------------------
if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )

	main( o_args )

	sys.exit( 0 )

