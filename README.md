# Cadavreski py

Un test de cadavre exquis en python.
Toutes les pull request (PR) sont les bienvenues !

## Règles du jeu :
L'objectif est de faire un programme qui fait quelque chose.
Votre PR peut contenir 1 fonction maximum.
Je n'accepterai pas 2 PR de suite de la même personne.

## Suggestions de code :
 - le squelette d'une classe = nom + mini doc
 - le test d'une fonction et laissez la fonction pour une autre personne (façon dirigée par les tests _TDD).